import { combineReducers } from 'redux';

import { formProdutosReducer } from './formProdutosReducer';
import { produtosReducer } from './produtosReducer';

export const rootReducer = combineReducers({
    produtosReducer,
    formProdutosReducer
});