import { PRODUTOS_REFRESH_REQUEST, PRODUTOS_REFRESH_INSERT, PRODUTOS_REFRESH_UPDATE, PRODUTOS_REFRESH_DELETE, PRODUTOS_REQUEST, PRODUTOS_SUCCESS } from '../actions/actionTypes';

const initialState = {
    registros: [],
    atualizando: false,
    loading: false,
};

export const produtosReducer = (state = initialState, action) => {
    switch (action.type) {

        /*** LISTANDO ITEMS ***/
        case PRODUTOS_REQUEST:
            return {
                ...state,
                loading: true,
                registros: [],
            };

        case PRODUTOS_SUCCESS:
            return {
                ...state,
                loading: false,
                registros: ordenar(action.registros),
            };

        /*** ATUALIZANDO ITEMS ***/
        case PRODUTOS_REFRESH_REQUEST:
            return {
                ...state,
                atualizando: true,
            };

        case PRODUTOS_REFRESH_INSERT:
        case PRODUTOS_REFRESH_UPDATE:
            let registrosRefresh = [...state.registros.filter(regAtual => { return regAtual._id !== action.item._id }), action.item]

            return {
                atualizando: false,
                registros: ordenar(registrosRefresh),
            };

        case PRODUTOS_REFRESH_DELETE:
            let registrosRefreshDelete = [...state.registros.filter(regAtual => { return regAtual._id !== action.id })]

            return {
                atualizando: false,
                registros: ordenar(registrosRefreshDelete),
            };

        default:
            return state;
    }
};

const ordenar = (colDados) => {
    return colDados.sort(function (item1, item2) {
        return item1['titulo'] - item2['titulo']
    })
}