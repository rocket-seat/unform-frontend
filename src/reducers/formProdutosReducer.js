import { PRODUTOS_GET_REQUEST, PRODUTOS_GET_SUCCESS, PRODUTOS_SAVE_REQUEST, PRODUTOS_UPDATE_SUCCESS, PRODUTOS_INSERT_SUCCESS } from '../actions/actionTypes';

const initialState = {
    item: {},
    loading: false
};

export const formProdutosReducer = (state = initialState, action) => {
    switch (action.type) {
        case PRODUTOS_GET_REQUEST:
            return {
                ...state,
                loading: true,
                item: {},
            };

        case PRODUTOS_GET_SUCCESS:
            return {
                ...state,
                loading: false,
                item: action.item,
            };

        case PRODUTOS_SAVE_REQUEST:
            return {
                ...state,
                loading: true,
            };

        case PRODUTOS_UPDATE_SUCCESS:
            return {
                ...state,
                loading: false,
                item: {},
            };

        case PRODUTOS_INSERT_SUCCESS:
            return {
                ...state,
                loading: false,
                item: {},
            };

        default:
            return state;
    }
};