import { PRODUTOS_GET_REQUEST, PRODUTOS_GET_SUCCESS, PRODUTOS_REFRESH_REQUEST, PRODUTOS_REFRESH_INSERT, PRODUTOS_REFRESH_DELETE, PRODUTOS_REFRESH_UPDATE, PRODUTOS_REQUEST, PRODUTOS_SUCCESS, PRODUTOS_SAVE_REQUEST, PRODUTOS_UPDATE_SUCCESS, PRODUTOS_INSERT_SUCCESS } from './actionTypes';

import { api } from '../services/api'

export const refreshProdutos = (type, item) => {
    return dispatch => {
        dispatch({ type: PRODUTOS_REFRESH_REQUEST })

        setTimeout(() => {
            if (type === 'new') {
                dispatch({ type: PRODUTOS_REFRESH_INSERT, item: item });
            }

            if (type === 'upd') {
                dispatch({ type: PRODUTOS_REFRESH_UPDATE, item: item });
            }

            if (type === 'del') {
                dispatch({ type: PRODUTOS_REFRESH_DELETE, id: item });
            }
        }, 1000)
    }
};

export const saveProduto = (id, item) => {
    return dispatch => {
        dispatch({ type: PRODUTOS_SAVE_REQUEST })

        if (id) {
            api.put('/produtos/' + id, item).then((res) => {
                dispatch({ type: PRODUTOS_UPDATE_SUCCESS });
            })
        } else {
            api.post('/produtos/', item).then((res) => {
                dispatch({ type: PRODUTOS_INSERT_SUCCESS });
            })
        }
    }
};

export const delProdutos = (item) => {
    return dispatch => {
        dispatch({ type: PRODUTOS_REFRESH_REQUEST })
        api.delete('/produtos/' + item._id)
    }
};

export const getProdutoID = (id) => {
    return dispatch => {
        dispatch({ type: PRODUTOS_GET_REQUEST })

        api.get('/produtos/' + id).then((res) => {
            dispatch({ type: PRODUTOS_GET_SUCCESS, item: res.data });
        })
    }
};

export const getProdutos = () => {
    return dispatch => {
        dispatch({ type: PRODUTOS_REQUEST })

        setTimeout(() => {
            api.get('/produtos').then((res) => {
                dispatch({ type: PRODUTOS_SUCCESS, registros: res.data.docs });
            })
        }, 1500)
    }
};
