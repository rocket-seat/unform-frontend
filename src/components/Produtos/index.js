import React, { Component } from 'react'
import socket from 'socket.io-client'

import { ioURL } from '../../services/api'

import { bindActionCreators } from 'redux';
import { refreshProdutos, getProdutos, saveProduto, delProdutos } from '../../actions';
import { connect } from 'react-redux';

import imgLoading from '../../assets/images/loading.gif'
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import { withStyles } from '@material-ui/core/styles';
import compose from 'recompose/compose';

class Produtos extends Component {

    async componentDidMount() {
        this.subscribeToNewFiles();
        await this.props.getProdutos();
    }

    subscribeToNewFiles = () => {
        const io = socket(ioURL)

        io.on('newProduto', data => {
            this.props.refreshProdutos('new', data);
        })

        io.on('updProduto', data => {
            this.props.refreshProdutos('upd', data);
        })

        io.on('delProduto', id => {
            this.props.refreshProdutos('del', id);
        })
    }

    novoProduto = () => {
        this.props.history.push('/formProdutos')
    }

    editarProduto = (item) => {
        this.props.history.push('/formProdutos/' + item._id)
    }

    removerProduto = (item) => {
        this.props.delProdutos(item);
    }

    render() {
        const { classes } = this.props;
        const { loading, atualizando, registros } = this.props.produtos;

        return (
            <Paper className={classes.root} elevation={1}>
                <Typography variant="h5" component="h3">
                    Gerenciamento de Produtos
                </Typography>

                <Typography component="span">
                    <p>
                        <Button variant="contained" color="primary" onClick={() => this.novoProduto()}>
                            <Icon>add</Icon>
                            Novo
                        </Button>
                    </p>

                    {atualizando && <div>Recebendo atualizações...</div>}

                    {loading && <img src={imgLoading} />}

                    {!loading &&
                        <List component="nav">
                            {registros.length > 0 && registros.map((item, i) => (
                                <ListItem button key={i} onClick={() => this.editarProduto(item)}>
                                    <ListItemText primary={item.titulo} secondary={item.descricao} />

                                    <ListItemSecondaryAction>
                                        <IconButton aria-label="Delete" onClick={() => this.removerProduto(item)}>
                                            <Icon>delete_forever</Icon>
                                        </IconButton>
                                    </ListItemSecondaryAction>
                                </ListItem>
                            ))}
                        </List>
                    }
                </Typography>
            </Paper>
        )
    }
}

const styles = theme => ({
    root: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
});

const mapStateToProps = state => ({ produtos: state.produtosReducer });
const mapDispatchToProps = dispatch => bindActionCreators({ refreshProdutos, getProdutos, saveProduto, delProdutos }, dispatch);

export default compose(withStyles(styles, { name: 'Produtos' }), connect(mapStateToProps, mapDispatchToProps))(Produtos)