import React, { Component } from "react";
import { Form, Input } from "@rocketseat/unform";

import { bindActionCreators } from 'redux';
import { saveProduto, getProdutoID } from '../../actions';
import { connect } from 'react-redux';

import imgLoading from '../../assets/images/loading.gif'
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import './style.css';
import { withStyles } from '@material-ui/core/styles';
import compose from 'recompose/compose';

class FormProdutos extends Component {

    async componentDidMount() {
        const id = this.props.match.params.id;

        if (id) {
            await this.props.getProdutoID(id)
        }
    }

    salvar = async (data) => {
        const id = this.props.match.params.id;
        await this.props.saveProduto(id, data)
        this.voltar();
    }

    voltar = () => {
        this.props.history.push('/')
    }

    render() {
        const { classes } = this.props;
        const { item, loading } = this.props.produto

        return (
            <Paper className={classes.root} elevation={1}>
                <Typography variant="h5" component="h3">
                    Detalhes do Produto
                </Typography>

                <Typography component="span">
                    <React.Fragment>
                        {loading && <img src={imgLoading} />}

                        {!loading &&
                            <Form onSubmit={this.salvar} initialData={item}>

                                <div className="div-float">
                                    <Input name="titulo" />
                                    <label>Titulo</label>
                                </div>

                                <br />

                                <div className="div-float">
                                    <Input name="descricao" />
                                    <label>Descrição</label>
                                </div>

                                <br />

                                <Button variant="contained" color="default" onClick={() => this.voltar()}>
                                    <Icon>keyboard_return</Icon>
                                    Voltar
                                </Button>

                                <Button variant="contained" color="primary" className="green" type="submit">
                                    <Icon>save</Icon>
                                    Salvar
                                </Button>
                            </Form>
                        }
                    </React.Fragment>
                </Typography>
            </Paper>
        );
    }
}

const styles = theme => ({
    root: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
});

const mapStateToProps = state => ({ produto: state.formProdutosReducer });
const mapDispatchToProps = dispatch => bindActionCreators({ saveProduto, getProdutoID }, dispatch);

export default compose(withStyles(styles, { name: 'FormProdutos' }), connect(mapStateToProps, mapDispatchToProps))(FormProdutos)