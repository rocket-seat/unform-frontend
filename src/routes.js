import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import FormProdutos from './components/FormProdutos'
import Produtos from './components/Produtos'

const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={Produtos} />
            <Route exact path="/formProdutos" component={FormProdutos} />
            <Route path="/formProdutos/:id" component={FormProdutos} />
        </Switch>
    </BrowserRouter>
)

export default Routes